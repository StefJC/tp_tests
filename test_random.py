import random
import unittest

class RandomTest(unittest.TestCase):
	"""Test case utilisé pour tester les fonctions du module 'random'."""

	def setUp(self):
		"""Initialisation des tests."""
		self.liste = list(range(10))

	def test_choice(self):
		"""Teste le fonctionnement de la fonction 'random.choice'."""
		elt = random.choice(self.liste)
		self.assertIn(elt, self.liste)

	def test_shuffle(self):
		"""Teste le fonctionnement de la fonction 'random.shuffle'."""
		random.shuffle(self.liste)
		self.liste.sort()
		self.assertEqual(self.liste, list(range(10)))

	def test_sample(self):
		"""Teste le fonctionnement de la fonction 'random.sample'."""
		extrait = random.sample(self.liste, 5)
		for element in extrait:
			self.assertIn(element, self.liste)

	def test_sample_check_exception(self):
		"""Teste le fonctionnement de la fonction 'random.sample' dans 
			un cas d'appel incorrect
		"""
		with self.assertRaises(ValueError):
			random.sample(self.liste, 20)


	def test_sample_check_exception2(self):
		"""Teste le fonctionnement de la fonction 'random.sample' dans 
			un cas d'appel incorrect, seconde version
		"""
		self.assertRaises(ValueError, random.sample, self.liste, 20)


	def tearDown(self):
		"""Libération des ressources, fermeture des fichiers eventuellement ouverts dans setUp."""

	def test_choice_erroneous_test(self):
		"""Teste très mal le fonctionnement de la fonction 'random.choice'."""
		elt = random.choice(self.liste)
		self.assertIn(elt, range(10))

	def test_crashing_test(self):
		"""Teste et crashe sur une division par zéro."""
		self.assertEqual(6, 6 / 1)
        
    #affichage de 'error' pour la division par zéro et de 'fail' pour le test
    #de la fonction random.choice
    # 'error' indique qu'il ne peut pas effectuer le test du fait que la division
    #par zero est impossible 
    # 'fail' indique que le test demandé n'est pas adéquat puisqu'il cherche
    #sur une suite de chiffre entre 1 et 10 au hasard (changement de la
    #liste a, b, c par range(10)))
    


