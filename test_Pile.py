#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import unittest

class Pile:
    def __init__(self):
        self.pile = []
        
    def estVide(self):
        return self.pile == []

    def empiler(self, e):
        self.pile.append(e)

    def depiler(self):
        if self.estVide():
            raise IndexError("attempt to pop from empty stack")
        return self.pile.pop()

    def sommet(self):
        if self.estVide():
            raise IndexError("attempt to read top of empty stack")
        return self.pile[-1]

class PileTest(unittest.TestCase):
    """Test case utilisé pour tester les fonctions depiler et sommet."""

    def test_depiler(self):
        """Test exception si la pile est vide"""
        self.assertRaises(IndexError, Pile.depiler, p)
    
    def test_sommet(self):
        """Test exception pile vide si demande le sommet"""
        self.assertRaises(IndexError, Pile.sommet, p)
    
    def test_estVide(self):
        """Test si une pile venant d'être créée est vide"""
        self.assertTrue(p.estVide())
    
    def test_empiler_nonvide(self):
        """Test si une pile après avoir empilé est non vide"""
        p = Pile()
        p.empiler('toto')
        self.assetFalse(p.estVide())
        


if __name__ == "__main__": # ne pas toucher au code ci-dessous
    p = Pile()
    for i in range(5):
        p.empiler(i)
    while not p.estVide():
        print(p.depiler())
    try:
        p.depiler()
    except IndexError as e:
        print(e)
    try:
        print(p.sommet())
    except IndexError as e:
        print(e)
