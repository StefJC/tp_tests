#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import unittest


class File:
    def __init__(self):
        self.file = []
         
    def estVide(self):
        return self.file == []

    def enfiler(self, e):
        self.file.append(e)

    def defiler(self):
        if self.estVide():
            raise IndexError("attempt to remove element from empty queue")
        return self.file.pop(0)

    def tete(self):
        if self.estVide():
            raise IndexError("attempt to read head of empty queue")
        return f[0]



class FileTest(unittest.TestCase):
    def test_estVide(self):
        """Test si une file venant d'être créée est vide"""
        p = File()
        self.assertTrue(p.estVide())
    
    def test_enfiler_nonvide(self):
        """Test si une file après avoir enfilé est non vide"""
        p = File()
        p.enfiler('tata')
        self.assetFalse(p.estVide())

    def test_element_surVide(self):
        """Test sur file vide si l'element en tête est l'élément"""
        p = File()
        elt = 'tata'
        self.assertEqual(elt, p.tete())
    
    def test_element_surNonVide(self):
        """Test sur file vide si l'element en tête est l'élément"""
        p = File()
        elt = 'tata'
        self.assertNotEqual(elt, p.tete())




if __name__ == "__main__": # ne pas toucher au code ci-dessous
    f = File()
    for i in range(5):
        f.enfiler(i)
    while not f.estVide():
        print(f.defiler())
    try:
        f.defiler()
    except IndexError as e:
        print(e)
    try:
        print(f.tete())
    except IndexError as e:
        print(e)

